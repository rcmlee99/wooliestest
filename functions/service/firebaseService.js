const firebase = require("firebase");
require("firebase/auth");
const functions = require("firebase-functions");

if (process.env.NODE_ENV === "production") {
  // Initialize Cloud Firestore through Firebase
  var configFirebase = {
    apiKey: functions.config().fb.apikey,
    authDomain: functions.config().fb.authdomain,
    projectId: functions.config().fb.projectid,
  };
  firebase.initializeApp(configFirebase);
}

const getCurrentUser = (accessToken) => {
  return new Promise((resolve, reject) => {
    firebase
      .auth()
      .signInWithCustomToken(accessToken)
      .then(() => {
        resolve(firebase.auth().currentUser);
        return;
      })
      .catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.message);
        reject(error);
      });
  });
};

module.exports = {
  getCurrentUser,
};
