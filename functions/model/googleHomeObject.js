const {
  BasicCard,
  SignIn,
  SimpleResponse,
  Suggestions,
} = require("actions-on-google");

const signInCardObject = () => {
  return new SignIn(
    "You need to link your Account with Google Assistant App. Follow the link in the Google Assistant App to continue"
  );
};

const splashCardObject = () => {
  const cardObject = new BasicCard({
    text: `Welcome to Wollies`,
    display: "CROPPED",
  });
  return cardObject;
};

const sendConvObject = (conv, resp, suggestion) => {
  conv.ask(new SimpleResponse({ speech: resp.speech, text: resp.text }));
  if (conv.surface.capabilities.has("actions.capability.SCREEN_OUTPUT")) {
    if (suggestion) {
      conv.ask(new Suggestions(suggestion));
    }
  }
};

module.exports = {
  signInCardObject,
  splashCardObject,
  sendConvObject,
};
