"use strict";

const functions = require("firebase-functions");
const { dialogflow } = require("actions-on-google");
const { getCurrentUser } = require("./service/firebaseService");
const {
  signInCardObject,
  sendConvObject,
} = require("./model/googleHomeObject");

// Initialize Dialogflow thru Actions-On-Google
const app = dialogflow({
  debug: false,
});

const dialogflowhttp = functions.https.onRequest(app);
const dialogflowtest = app;

const helpText = `I can help you with product availability. What store and product can I help you with today?`;
var resp = {
  speech: `<speak>${helpText}</speak>`,
  text: helpText,
};
var suggestion = [];

const resetDataAndContext = (conv) => {
  conv.contexts.set("welcome-context", 0);
  conv.contexts.set("welcome-context", 0);
  conv.data.product = null;
  conv.data.store = null;
};

app.intent("Default Welcome Intent" || "actions.intent.MAIN", async (conv) => {
  const queryResult = conv.body.queryResult;
  console.log("Welcome :::", JSON.stringify(queryResult));
  console.log("User :::", JSON.stringify(conv.user));
  let accessToken = conv.user.access.token;

  // If account linked, greet the user displayName else ask the name alternatively ask for account linking
  // if (accessToken === undefined) {
  //   conv.ask(signInCardObject());
  // }
  if (accessToken) {
    let firebaseUser = await getCurrentUser(accessToken);
    console.log("firebaseUser :::", JSON.stringify(firebaseUser));
    conv.data.firebaseuser = firebaseUser;
    conv.ask(
      `Hi ${firebaseUser.displayName}, I can help you with product availability. What store and product can I help you with today?`
    );
  } else {
    conv.ask("Hi, who am I speaking with?");
  }
});

app.intent("NameIntent", (conv) => {
  const queryResult = conv.body.queryResult;
  console.log("NameIntent :::", JSON.stringify(queryResult));
  console.log(
    `queryTextNameIntent=${JSON.stringify(queryResult.queryText)}, Confidence=${
      queryResult.intentDetectionConfidence
    }`
  );
  const [givenName, lastName] = [
    queryResult.parameters["given-name"],
    queryResult.parameters["last-name"],
  ];
  if (queryResult.allRequiredParamsPresent) {
    conv.data.givenName = givenName;
    conv.data.lastName = lastName;
    const talk = `Hi ${givenName} ${lastName}, I can help you with product availability. What store and product can I help you with today?`;
    resp = {
      speech: `<speak>${talk}</speak>`,
      text: talk,
    };
    suggestion = ["Bananas", "Apples", "Surry Hills", "Parramatta"];
    sendConvObject(conv, resp, suggestion);
  }
});

app.intent("ProductIntent", (conv) => {
  const queryResult = conv.body.queryResult;
  console.log("ProductIntent :::", JSON.stringify(queryResult));
  console.log(
    `queryTextProductIntent=${JSON.stringify(
      queryResult.queryText
    )}, Confidence=${queryResult.intentDetectionConfidence}`
  );
  console.log("conv.data :::", JSON.stringify(conv.data));
  const product = queryResult.parameters["product"];
  if (queryResult.allRequiredParamsPresent) {
    conv.data.product = product;
    // If store and product fulfilled, call API and construct response
    if (conv.data.store) {
      const talk = `Thanks, we have 30 ${product} in stock at our ${conv.data.store} store. Was there anything else I can help you with?`;
      resp = {
        speech: `<speak>${talk}</speak>`,
        text: talk,
      };
      // Clear the Store and Product Data for Next Conversation
      resetDataAndContext(conv);
    } else {
      const talk = `Sure, and which Store was that for?`;
      resp = {
        speech: `<speak>${talk}</speak>`,
        text: talk,
      };
      suggestion = ["Surry Hills", "Parramatta"];
    }
  }
  sendConvObject(conv, resp, suggestion);
});

app.intent("StoreNameIntent", (conv) => {
  const queryResult = conv.body.queryResult;
  console.log("StoreNameIntent :::", JSON.stringify(queryResult));
  console.log(
    `queryTextStoreNameIntent=${JSON.stringify(
      queryResult.queryText
    )}, Confidence=${queryResult.intentDetectionConfidence}`
  );
  console.log("conv.data :::", JSON.stringify(conv.data));
  const store = queryResult.parameters["store"];
  if (queryResult.allRequiredParamsPresent) {
    conv.data.store = store;
    // If store and product fulfilled, call API and construct response
    if (conv.data.product) {
      const talk = `Thanks, we have 30 ${conv.data.product} in stock at our ${store} store. Was there anything else I can help you with?`;
      resp = {
        speech: `<speak>${talk}</speak>`,
        text: talk,
      };
      // Clear the Store and Product Data for Next Conversation
      resetDataAndContext(conv);
    } else {
      const talk = `Sure, and which product was that for?`;
      resp = {
        speech: `<speak>${talk}</speak>`,
        text: talk,
      };
      suggestion = ["Bananas", "Apples"];
    }
  }
  sendConvObject(conv, resp, suggestion);
});

app.intent("CancelIntent" || "actions.intent.CANCEL", (conv) => {
  console.log(
    `queryTextCancel=${JSON.stringify(
      conv.body.queryResult.queryText
    )}, Confidence=${conv.body.queryResult.intentDetectionConfidence}`
  );
  conv.close("I am here if you need. Have a great day.");
});

app.intent("HelpIntent", (conv) => {
  console.log("convQuery :::", JSON.stringify(conv.query));
  console.log("convArg :::", JSON.stringify(conv.arguments));
  conv.ask(helpText);
});

app.intent("Unrecognized Deep Link Fallback", (conv) => {
  console.log("convQuery :::", JSON.stringify(conv.query));
  console.log("convArg :::", JSON.stringify(conv.arguments));
  conv.ask(helpText);
});

app.catch((conv, error) => {
  console.error("error :::", JSON.stringify(error));
  conv.close(`Something went wrong, please try again later.`);
});

module.exports = {
  dialogflowhttp,
  dialogflowtest,
};
