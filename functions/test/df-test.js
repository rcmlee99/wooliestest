/* eslint-disable prefer-arrow-callback */
// This file contains unit tests for a Dialogflow agent.
"use strict";

const { expect } = require("chai");
const { DialogflowApiFactory } = require("./lib/df-api");
const uuid = require("uuid");
const test = require("ava");

/* ====== Substitute the following variables (START) ===== */
const projectId = "wollies-9wel";
// Make sure the service account has "Dialogflow API Client" in GCP IAM
const pathToServiceAccount = "./key.json";
// The value itself doesn't matter (can be anything).
const sessionId = uuid.v1();
/* ====== Substitute the following variables (END) ======= */

const serviceAccount = require(pathToServiceAccount);

let dialogflow = undefined;

test.before(async function (t) {
  dialogflow = await DialogflowApiFactory.create({
    projectId: projectId,
    serviceAccount: serviceAccount,
    sessionId: sessionId,
  });
});

test.afterEach(async function (t) {
  await dialogflow.clearSession(sessionId);
});

test.serial("Bananas", async function (t) {
  const resJson = await dialogflow.detectIntent("Bananas");
  expect(resJson.queryResult).to.include.deep.keys("parameters");
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    product: "bananas",
  });
  expect(resJson.queryResult.intent.displayName).to.equal("ProductIntent");
  t.pass();
});

test.serial("Parramatta", async function (t) {
  const resJson = await dialogflow.detectIntent("Parramatta");
  expect(resJson.queryResult).to.include.deep.keys("parameters");
  // check that Dialogflow extracted required entities from the query.
  expect(resJson.queryResult.parameters).to.deep.equal({
    store: "parramatta",
  });
  expect(resJson.queryResult.intent.displayName).to.equal("StoreIntent");
  t.pass();
});

test.serial("Help", async function (t) {
  const resJson = await dialogflow.detectIntent("HelpIntent");
  expect(resJson.queryResult.fulfillmentText).to.equal(
    "I can help you with product availability. What store and product can I help you with today?"
  );
  expect(resJson.queryResult.intent.displayName).to.equal("Help");
  t.pass();
});
