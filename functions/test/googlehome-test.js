// This file contains unit tests for a Google Home Object.
const test = require("ava");
const { expect } = require("chai");
const {
  signInCardObject,
  splashCardObject,
} = require("../model/googleHomeObject");

test.serial("googleHomeObject signInCardObject", async (t) => {
  const result = await signInCardObject();
  expect(result).to.be.an("object");
  expect(result.intent).to.equal("actions.intent.SIGN_IN");
  expect(result.inputValueData.optContext).to.equal(
    "You need to link your Account with Google Assistant App. Follow the link in the Google Assistant App to continue"
  );
  t.pass();
});

test.serial("googleHomeObject splashCardObject", async (t) => {
  const result = await splashCardObject();
  expect(result).to.be.an("object");
  expect(result.formattedText).to.equal("Welcome to Wollies");
  expect(result.imageDisplayOptions).to.equal("CROPPED");
  t.pass();
});
